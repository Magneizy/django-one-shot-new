from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from .models import TodoItem, TodoList

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todo_lists/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todo_lists/create.html"
    fields = ["name"]
    success_url = reverse_lazy('todo_list')


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todo_lists/update.html"
    fields = ["name"]
    success_url = reverse_lazy('todo_list')


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todo_lists/delete.html"
    success_url = reverse_lazy('todo_list')


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todo_items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy('todo_list')


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todo_items/update.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy('todo_list')